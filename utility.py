'''
    Cutter Coryell
    Not quite sure how to format this header correctly yet. Still a draft.
    
    An implementation of p_any and the greedy solution that takes telescope range into account.
    '''

import numpy as np
import healpy
from bayestar.plan import greedy_sorted_solution, visible_mask
from bayestar.convolve.multipole_openmp import convolve_altaz
from bayestar.convolve.examples import make_rect
from bayestar import sites, coord, misc
from bayestar.convolve.spatial import rotate_map, rotate_indices


def p_any(tel_keys, pointings, skymap, max_dists=None, visibilities=None, GW_range=200.0, gpstime=None):
    """
        This p_any is idealized and relies upon some assumptions:
        * Taking two pictures of the same patch of sky with two identical
        telescopes is no better than taking one picture of the patch
        of sky with one of the telescopes.
        * If a telescope can detect an EM source out to the range of the
        GW detector network, then being able to detect at a further
        range does not improve chance of detection.
        * If the EM source is within the range of a telescope, and in the field
        of view of the telescope, then the probability of detecting the
        source with that telescope is a function of the sky position of
        the source only, called the visibility. Ideally the visibility is
        1 everywhere, except for in the direction of the Earth and the
        daytime sky, where it is 0.
        
        Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        pointings: a list of the same length as 'tel_keys', giving the pointing
        direction for each respective telescope.  Each pointing direction
        can be of the form (theta, phi) for an altazimuth rotation, or of
        the form (Phi_2, Theta, Phi_1) for a general Euler angle rotation.
        skymap: healpy array for the skymap.
        max_dists: a numpy array of the same length as 'tel_keys', giving the maximum
        detection range for each respective telescope. This maximum range is
        a function of the telescope's limiting magnitude and the intensity
        of the predicted source lightcuve at that time. Units are Mpc.
        visibilities: a list of the same length of 'tel_keys', giving the
        healpy array representing the visibility map for each respective
        telescope, a skymap with values between 1 for full visibility and 0
        for no visibility at each point.
        This includes the mask for moon, Earth, and day sky positions, as
        well as other local reductions of visibility.
        GW_range: the maximum range of the GW detector network, for determining
        the total search volume. Units are Mpc.
        """
    
    nside = healpy.npix2nside(skymap.size)
    npix = healpy.nside2npix(nside)
    num_tels = len(tel_keys)
    
    # the following is so we don't mess with the original arguments. it copies each thing,
    # making sure we have np arrays where we want them
    tel_keys = list(tel_keys)
    pointings = list(pointings)
    skymap = np.array(skymap).copy()
    
    # by default, either set full visibility or mask it with the earth and the daytime
    # sky, depending on whether or not we know the time
    if visibilities == None:
        if gpstime == None:
            visibilities = [np.ones(len(skymap)) for i in range(num_tels)]
        else:
            visibilities = [visible_mask(sites[key], gpstime, nside) for key in tel_keys]
    
    # if they provide visibilities, copy it so we don't mess with it
    else:
        visibilities = list(visibilities)
    
    # by default assume ranges are equal to the GW detector network range
    if max_dists == None:
        max_dists = GW_range * np.ones(num_tels)
    
    # if they provide a list of max_dists, copy it, and reduce anything
    # over the GW_range to the GW_range
    else:
        max_dists = np.array(max_dists).copy() 
        # assign all telescope ranges that are longer than the GW detector
        # network range to the GW range. in this ideal case, having a range
        # farther than the GW range does not increase chances of detection,
        # so we can reduce it to the GW range without changing anything.
        for i, max_dist in enumerate(max_dists):
            if max_dist > GW_range:
                max_dists[i] = GW_range
        # if max_dists is not default, then
        #   sort telescopes (and properties) by order of increasing maximum distance
        tel_keys, max_dists, (pointings, visibilities) = sort_telescopes(tel_keys, max_dists, (pointings, visibilities),
                                                                         method='distance', increasing=True)
    
    coefficients = []
    convolutions = []
    
    for i in range(num_tels+1):
        
        coefficient = calculate_coefficient(i, max_dists, GW_range)
        coefficients.append(coefficient)
        
        if coefficient == 0.0:
            convolution == 0.0 # This is a dummy value; coefficient * convolution for
        # this index will be 0.0 no matter what the convolution is. Note that, for
        # networks of real telescopes, coefficient will only equal 0.0 if two telescopes
        # have the same limiting magnitude, which is much rarer than it is in testing.
        # Any time all telescopes have the same limiting magnitude, p_any() will run
        # much faster than in general; this should be kept in mind.
        
        else:
            convolution = calculate_convolution(i, tel_keys, pointings, skymap, visibilities, nside)
        convolutions.append(convolution)
    
    prob = 1.0
    
    for i in range(num_tels+1):
        prob -= coefficients[i] * convolutions[i]
    
    return prob


def greedy_solution(tel_keys, skymap, max_dists=None, visibilities=None, GW_range=200.0, gpstime=None, lmax=None):
    """
        Employs a greedy algorithm to compute a solution to the optimization
        problem of pointing several telescopes at the same sky.  It returns a list of
        pointing directions for each telescope in tel_keys, as well as the total
        probability encolsed by all fields of view.
        
        Parameters:
        tel_keys:  list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        lmax: band limit for the multipole convolution algorithm.  The output
        of the convolution is sampled on an equiangular grid with angular
        spacing of (2*pi)/(2*lmax + 1)
        """
    
    nside = healpy.npix2nside(skymap.size)
    num_tels = len(tel_keys)
    used_telescopes = []
    pointings = []
    skymap = np.copy(skymap)
    
    if max_dists == None:
        max_dists = GW_range * np.ones(num_tels)
    
    if visibilities == None:
        if gpstime == None:
            visibilities = [np.ones(len(skymap)) for i in range(num_tels)]
        else:
            visibilities = [visible_mask(sites[key], gpstime, nside) for key in tel_keys]
    
    for i, key in enumerate(tel_keys):
        
        site = sites[key]
        max_dist = max_dists[i]
        if max_dist > GW_range:
            max_dist = GW_range
        vis = visibilities[i]
        
        kern = make_rect(nside, 0.5 * site['w'], 0.5 * site['h'], deg=True)
        masked_skymap = skymap * vis
        
        # If none of the skymap is visible to this telescope, skip it.
        if not masked_skymap.any():
            continue
        
        conv = convolve_altaz(masked_skymap, kern, lmax=lmax)
        max_idx = conv.argmax()
        thm, phm = healpy.pix2ang(nside, max_idx)
        pointings.append((thm, phm))
        used_telescopes.append(key)
    
        skymap *= (1 - (max_dist**3/GW_range**3) * vis * rotate_map(kern, phm, thm, 0))
        
    return used_telescopes, pointings, p_any(used_telescopes, pointings, skymap, max_dists, visibilities, GW_range, gpstime)




def sort_telescopes(tel_keys, max_dists, tel_properties=[], method='distance', increasing=True):
    """
    Sorts all telescopes and properties by order of maximum distance,
        search volume, or field of view area.
    
    Returns a sorted copy of tel_keys, max_dists, and a copy of tel_properties
        with all sublists sorted. Hence, if tel_properties is not specified,
        the third return item will be an empty list (if this is the case,
        this function should be called like tel_keys, max_dists, _ = sort_telescopes()).

    Note: Everything inside tel_properties will be returned as a list, not as
        numpy arrays.
        
    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        max_dists: a list of the same length as 'tel_keys', giving the maximum
            detection range for each respective telescope. This maximum range is
            a function of the telescope's limiting magnitude and the intensity
            of the predicted source lightcuve at that time. Units are Mpc.
        tel_properties: a sequence of property lists. Each property list becomes
            internally sorted. This can be empty, in which case tel_keys and
            max_dists are the only telescope properties that will be sorted.
            Examples of property lists would be the visibility maps associated
            with each telescope, and the pointings associated with each telescope.
        method: 'distance', 'volume', or 'fov', specifying whether the telescopes
            and respective properties will be sorted by maximum distance,
            search volume, or field of view area.
        increasing: True if the telescopes are to be sorted in order of increasing
            distance/volume, and False if decreasing.
    """
    
    sorted_keys = []
    sorted_dists = []
    
    sorted_props = []
    for prop in tel_properties:
        sorted_props.append([])
    
    # sorting_list is the list of comparables by which we sort the rest
        # of the telescope properties
    if method == 'distance':
        sorting_list = max_dists
    elif method == 'volume':
        sorting_list = generate_search_volumes(tel_keys, max_dists)
    elif method == 'fov':
        sorting_list = generate_fov_areas(tel_keys)
    else:
        raise ValueError("method must be either 'distance', 'volume', or 'fov'")
    
    sorted_no_redunds = np.array(sorted(list(set(sorting_list)), reverse=(not increasing)))
    # the above trick gets rid of redundant items in sorting_list

    for item in sorted_no_redunds:
        indices = np.where(sorting_list == item)[0]
        # gives a list of places where item is found in sorting_list
        for i in indices:
            sorted_keys.append(tel_keys[i])
            sorted_dists.append(max_dists[i])
            for j, prop in enumerate(tel_properties):
                sorted_props[j].append(prop[i])

    return sorted_keys, sorted_dists, sorted_props


def generate_max_dists(tel_keys, source_absmag):
    """
    Computes the maximum ranges (in Mpc) of each of the telescopes
        in tel_keys, based on the absolute magnitude of the
        EM source.
        
    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        source_absmag: the absolute magnitude of the brightness of
        the source. Can either be a number
        or a list of floats of the same length as tel_keys.
    """
    
    if isinstance(source_absmag, int) or isinstance(source_absmag, float):
        return [10**((sites[key]['mag'] - source_absmag)/5.0 - 5) for key in tel_keys]

    elif isinstance(source_absmag, list) and len(source_absmag) == len(tel_keys):
        return [10**((sites[key]['mag'] - absmag)/5.0 - 5) for key, absmag in zip(tel_keys, source_absmag)]


def generate_search_volumes(tel_keys, max_dists):
    """
    Computes the search volume of each telescope in tel_keys in cubic
        Mpc. This is found by multiplying the solid angle of the
        the telescope FOV by the cube of the telescope maximum
        distance and dividing by three (the idea being that this volume
        is the volume of a sphere of radius max_dist multiplied by the
        fraction of the total sky the telescope FOV encompasses).

    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        max_dists: a list of the same length as 'tel_keys', giving the maximum
            detection range for each respective telescope. This maximum range is
            a function of the telescope's limiting magnitude and the intensity
            of the predicted source lightcuve at that time. Units are Mpc.

    """
    areas = generate_fov_areas(tel_keys)
    return [1.0/3 * area * max_dist**3 for area, max_dist in zip(areas, max_dists)]
                        
                
def generate_fov_areas(tel_keys):
    """
    Computes the field of view area of each telescope in tel_keys in steradians.
    
    Parameters:
    tel_keys: list of strings specifying the telescopes to use.    
    """
    return [sites[key]['w']*sites[key]['h']*(np.pi/180)**2 for key in tel_keys]



############# Below are helper functions for p_any(). ##############

def calculate_coefficient(index, max_dists, GW_range):
    """
    Calculates the coefficients in Equation 5 of Telescope Network Utility
        Function (i.e., the C_i).
        
    Parameters:
        index: The 'index' of the coefficient. For purposes of this function,
            the 'index' of C_i is i-1.
        max_dists: a list of the maximum distances of the telescope network,
            sorted from least to greatest. Units are Mpc.
        GW_range: the maximum range of the GW detector network, for determining
            the total search volume. Units are Mpc.
    """
    
    num_tels = len(max_dists)
    
    if index == 0:
        smaller_dist = 0.0
    else:
        smaller_dist = max_dists[index-1]
    
    if index == num_tels:
        larger_dist = GW_range
    else:
        larger_dist = max_dists[index]
    
    return (larger_dist**3 - smaller_dist**3) / GW_range**3


def calculate_convolution(index, tel_keys, pointings, skymap, visibilities, nside):
    """
    Calculates the convolution integrals in Equation 5 of Telescope Network
        Utility Function (i.e., the I_i).
        
    Parameters:
        index: The 'index' of the coefficient. For purposes of this function,
            the 'index' of C_i is i-1.
        tel_keys: a list of telescope keys, sorted in order of increasing maximum
            distance.
        pointings: a list of the same length as 'tel_keys', giving the pointing
            direction for each respective telescope.  Each pointing direction
            can be of the form (theta, phi) for an altazimuth rotation, or of
            the form (Phi_2, Theta, Phi_1) for a general Euler angle rotation.
        visibilities: a list of the same length of 'tel_keys', giving the
            healpy array representing the visibility map for each respective
            telescope, a skymap with values between 1 for full visibility and 0
            for no visibility at each point.
            This includes the mask for moon, Earth, and day sky positions, as
            well as other local reductions of visibility.
    """
    
    num_tels = len(tel_keys)    
    
    if index == num_tels:
        return 1.0
    
    convo_terms = []
    
    for i in range(index, num_tels):
        
        (key, pnt, vis) = (tel_keys[i], pointings[i],
                           visibilities[i])
        site = sites[key]
        w = 0.5 * np.deg2rad(site['w'])
        h = 0.5 * np.deg2rad(site['h'])
        kern = make_rect(nside, w, h, deg=False, dtype=float)
        supported_radius = np.arcsin(np.sqrt(np.sin(w) ** 2 + np.sin(h) ** 2))
        
        if len(pnt) == 2:
            euler_rotation = (pnt[1], pnt[0], 0)
        elif len(pnt) == 3:
            euler_rotation = pnt
        else:
            raise ValueError('Elements of pointings must be tuples of length '\
                             +'2 or 3')
        
        supported_indices = misc.query_disc_inclusive(nside, pnt[0], pnt[1], supported_radius)
        rotated_indices = rotate_indices(supported_indices, nside, *euler_rotation)
        FOV = np.zeros(len(skymap))
        FOV[supported_indices] = kern[rotated_indices]
        convo_terms.append(np.ones(len(skymap)) - FOV * vis)
    
    product = skymap.copy()
    
    for convo_term in convo_terms:
        product *= convo_term
    
    return product.sum()