Documentation for client-server networking protocol in "Realworld", a prototype multiplayer computer game.

=== Low-Level Protocol ===

Messages are one of two forms:

* `MSGTYPE` space `MSGPARAM` newline
* `MSGTYPE` newline

== Client-to-Server Messages ==

`MSGTYPE` can currently be:

* `quit`: The client is attempting to gracefully quit. `MSGPARAM`, if present,
          is ignored.
* `input`: The client is signaling that there is relevant input from the
           player. `MSGPARAM` is of the form "x=`x`,y=`y`[,...]".
           `x` and `y` are numbers in [-1, 1] that correspond to the current
           spatial intentions of the player. ... are additional inputs,
           separated by commas, for example "input1,input2,input3".
* `pew`: The client believes they fired a shot!  `MSGPARAM` is required and of
         the form "`dst_x` `dst_y`", both numbers.

== Server-to-Client Messages ==

`MSGTYPE` can currently be:

* `kick`: The server is kicking the client from the session. `MSGPARAM`, if
          present, contains a message explaining the reason for the kick.
* `forget`: The server is telling the client that another client has left
            the game. `MSGPARAM` must be present; it is the index of the player
            that has left the game.
* `welcome`: The server has acknowledged that the client has been added to the
             game. `MSGPARAM`, if present, is ignored.
* `new_player`: The server is telling the client that another client has
                joined the game. `MSGPARAM`, if present, is ignored.
* `update_player`: The server is sending information about a player to the
                   client. `MSGPARAM` is required; it is of the form
                   `INDEX` `X` `Y` [x=`x`,y=`y`[,...]]
                   * `INDEX` is a positive integer corresponding to the index
                     of the player that is being updated
                   * `X` is the authoritative x-coordinate of the player
                   * `Y` is the authoritative y-coordinate of the player
                   * x=`x`,y=`y`[,...] is the same as the `MSGPARAM`
                     for the `input` client-to-server message.
* `pew`: Somebody shot something! `MSGPARAM` is required and of the form
         "`x` `y` `vx` `vy` `color`", all numbers.
* `pow`: A shot disappeared. `MSGPARAM` is the disappearing shot ID.
