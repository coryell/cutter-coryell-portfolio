-- Client-specific functions and data structures.

--- Sends user input to server, updates game state according
--- to server messages.
function client_action()
   receive_messages_from_server()
   send_input_to_server()
end

--- Pass client local input to the server.
function send_input_to_server()
   local message = "input"
   local t = {"x="..local_player.input.x, "y="..local_player.input.y}
   for k in pairs(local_player.input) do
      if k ~= "x" and k ~= "y" then
         t[#t+1] = k
      end
   end
   message = message .. " " .. table.concat(t,",")
   socket:Send(message.."\n")
end

local buffered_input
function receive_messages_from_server()
   -- Get all available input (well, up to a lot of available input) from the
   -- client.                                                                
   local input,e = socket:Receive(2048)
   if not input then
      if e == "not ready" then
         -- we're okay, the server just hasn't sent us data in a while
      else
         -- we're not okay! quit the game
         quit = true
      end
      return
   elseif buffered_input then
      -- Combine any already-buffered input with the new input.    
      input = buffered_input .. input
      buffered_input = nil
   end
   local actual_cutoff = 1 -- if no input is matched, cut off at the start
   for message,cutoff in input:gmatch("([^\n]+)\n()") do
      actual_cutoff = cutoff
      handle_message_from_server(message)
   end
   if actual_cutoff <= #input then
      -- save that fraction of a line to be handled later
      buffered_input = input:sub(actual_cutoff, -1)
   end
end

local message_handlers = {}

--- Server has kicked the client from the session, providing a reason.
function message_handlers.kick(reason)
   error(("kicked from server because '%s'"):format(tostring(reason)))
end

--- Server has signaled client that it no longer needs to keep track of
--- player `index`.
function message_handlers.forget(index)
   table.remove(players, index)
   for n=index,#players do
      players[n].index = n
   end
end

--- Server is signaling that the client has been added to the game and
--- should add itself to `players`.
function message_handlers.welcome()
   players[#players+1] = local_player
   local_player.index = #players
end

--- Server is signaling that the client should add another new player to the
--- game and should add it to `players`.
function message_handlers.new_player()
   players[#players+1] = {x=10, y=10, facing=0, index=#players+1, input={x=0,y=0}}
end

function message_handlers.update_player(param)
   local index,xp,yp,ix,iy,q = (param or ""):match("^([0-9]+) (%-?[0-9]*%.?[0-9]+) (%-?[0-9]*%.?[0-9]+) x=(%-?[01]?%.?[0-9]+),y=(%-?[01]?%.?[0-9]+)(.*)$")
   if not index or (#assert(q) > 0 and q:sub(1,1) ~= ",") then
      index,xp,yp = (param or ""):match("^([0-9]+) (%-?[0-9]+%.?[0-9]*) (%-?[0-9]+%.?[0-9]*)$")
      -- if any of them are nil, all will be
      if not index then
         print(param)
         error("malformed update_player message")
      end
   end
   local player = players[tonumber(index)]
   player.x, player.y = tonumber(xp), tonumber(yp)
   if ix then
      assert(player ~= local_player, "Server can't tell us what to do!")
      player.input = {} -- wipe away old inputs
      player.input.x = math.min(math.max(assert(tonumber(ix)),-1),1)
      player.input.y = math.min(math.max(assert(tonumber(iy)),-1),1)
      for extra in q:gmatch(",([^,]+)") do
         player.input[extra] = true
      end
   end
end

function message_handlers.pew(param)
   local x,y,vx,vy,color = (param or ""):match("^(%-?[0-9]*%.?[0-9]+) (%-?[0-9]*%.?[0-9]+) (%-?[0-9]*%.?[0-9]+) (%-?[0-9]*%.?[0-9]+) ([0-9]+)$")
   if not x then -- how do we handle malformed messages, again?
      print(param)
      error("malformed pew message")
   else
      projectiles[#projectiles+1] = {x = tonumber(x), y = tonumber(y),
         vx = tonumber(vx), vy = tonumber(vy),
         color = tonumber(color)}
   end
end

function message_handlers.pow(param)
   local index = (param or ""):match("^([0-9]+)$")
   table.remove(projectiles, index)
end

--- Handle a single message.
function handle_message_from_server(message)
   local msgtype,param = message:match("^([^ ]+) (.*)$")
   msgtype = msgtype or message
   if message_handlers[msgtype] then
      message_handlers[msgtype](param)
   else
      error(("unknown message type %q"):format(msgtype))
   end
end
