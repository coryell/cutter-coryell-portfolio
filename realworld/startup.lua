-- Code for the initial startup dialog.

local scui = require "scui"
local uranium = require "uranium"

screen = assert(SC.Construct("GraphicsDevice", 290, 125, {windowed=true,vsync=true,title="Dialogue"}))
sw,sh = screen:GetSize()
screen:SetCursor(uranium.cursor())

local widget = {
   x=0, y=0, w=sw, h=sh,
   type=uranium.box,
   filled=true,
   {x=20,y=15,w=250,h=20,type=uranium.button,text="Create Game",tag="create"},
   {x=20,y=55,w=250,h=20,type=uranium.field,text="00.000.000.000"},
   {x=20,y=85,w=250,h=20,type=uranium.button,text="Join Game",tag="join"}
}

scui.CompileWidget(widget)

local tag = scui.RunModal(widget, nil, uranium.cursor())

screen = assert(SC.Construct("GraphicsDevice", 640, 480, {windowed=true,vsync=true,title="RealWorld"}))
sw,sh = screen:GetSize()
screen:SetCursor(uranium.cursor())

if tag == "create" then
   host_game()
elseif tag == "join" then
   join_game(widget[2].text)
end
