-- Server-specific functions and data structures.

--- If there are any new clients waiting, add them to the game.
local function accept_new_clients()
   local new_client = listen:Accept()
   if new_client then
      local new_player = {x=10, y=10, facing=0, socket=new_client, index=#players+1, input={x=0,y=0}}
      for n=1,#players do
         server_send_new_player(players[n], new_player)
         server_send_new_player(new_player, players[n])
      end
      for n=1,#projectiles do
         server_send_pew(new_player, projectiles[n])
      end
      server_send_welcome(new_player)
      players[new_player.index] = new_player
      -- Accept any other connections that are waiting.
      return accept_new_clients()
   end
end

--- Receive and handle all incoming messages available on a given player's
--- socket.
local function receive_messages_from_player(player)
   if player == local_player then return end
   -- Get all available input (well, up to a lot of available input) from the
   -- client.
   local input,e = player.socket:Receive(2048)
   if not input then
      if e == "not ready" then
         -- we're okay, the player just hasn't sent us data in a while
      else
         -- we're not okay! mark this player to be cleaned up
         player.socket = nil
      end
      return
   elseif player.buffered_input then
      -- Combine any already-buffered input with the new input.
      input = player.buffered_input .. input
      player.buffered_input = nil
   end
   local actual_cutoff = 1 -- if no input is matched, cut off at the start
   for message,cutoff in input:gmatch("([^\n]+)\n()") do
      actual_cutoff = cutoff
      handle_message_from_player(player, message)
   end
   if actual_cutoff <= #input then
      -- save that fraction of a line to be handled later
      player.buffered_input = input:sub(actual_cutoff, -1)
   end
end

local message_handlers = {}

function message_handlers.quit(player)
   kick_player(player, "Good riddance!")
end

function message_handlers.input(player, param)
   local ix,iy,q = (param or ""):match("^x=(%-?[01]?%.?[0-9]+),y=(%-?[01]?%.?[0-9]+)(.*)$")
   if not ix or (#q > 0 and q:sub(1,1) ~= ",") then
      kick_player(player, "malformed input message")
   else
      -- handle input
      player.input = {} -- wipe away old inputs
      player.input.x = math.min(math.max(assert(tonumber(ix)),-1),1)
      player.input.y = math.min(math.max(assert(tonumber(iy)),-1),1)
      for extra in q:gmatch(",([^,]+)") do
         player.input[extra] = true
      end
   end
end

function message_handlers.pew(player, param)
   local dst_x,dst_y = (param or ""):match("^(%-?[0-9]*%.?[0-9]+) (%-?[0-9]*%.?[0-9]+)$")
   local src_x, src_y = player.x, player.y
   -- no suicides!
   if src_x == dst_x and src_y == dst_y then
      return
   end
   local dx, dy = dst_x - src_x, dst_y - src_y
   local velocity_factor = projectile_velocity / math.sqrt(dx*dx + dy*dy)
   local vx = dx * velocity_factor
   local vy = dy * velocity_factor
   local projectile = {x = src_x, y = src_y, vx = vx, vy = vy, color = player.index, lifetime = projectile_lifetime}
   projectiles[#projectiles+1] = projectile
   -- we have to send the message to all clients here
   for n=1,#players do
      local player = players[n]
      if player.socket and player ~= local_player then
         server_send_pew(player, projectile)
      end
   end
end
server_message_handlers_pew = message_handlers.pew

--- Han'le a single message.
function handle_message_from_player(player, message)
   if not player.socket then return end
   local msgtype,param = message:match("^([^ ]+) (.*)$")
   msgtype = msgtype or message
   if message_handlers[msgtype] then
      message_handlers[msgtype](player, param)
   else
      kick_player(player, "unknown message type %q", msgtype)
   end
end

--- Tell client to walk the plank! Even give them an optional reason why.
function kick_player(player, format, ...)
   assert(player ~= local_player)
   if player.socket == nil then return end
   local msg = "kick "..format:format(...):gsub("\\\n","\\n").."\n"
   player.socket:Send(msg)
   player.socket = nil
end

--- Receives input from players, processes it, and updates player states.
local function sync_with_clients()
   for i=1,#players do
      receive_messages_from_player(players[i])
   end
   for i=#players,1,-1 do
      if not players[i].socket and players[i] ~= local_player then
         for j=1,#players do
            if j ~= i then
               server_send_forget_player(players[j], players[i])
            end
         end
         table.remove(players, i)
         for j=i,#players do
            players[j].index = j
         end
      end
   end
   for i=1,#players do
      update_world_for_player(players[i])
   end
end

--- Tell `target` client that `new_player` has joined the session.
function server_send_new_player(target, new_player)
   if target == local_player then return end
   target.socket:Send "new_player\n"
end

--- Let `target` know it has joined the session.
function server_send_welcome(target)
   assert(target ~= local_player)
   target.socket:Send "welcome\n"
end

--- Tell `target` that `victim` disconnected.
function server_send_forget_player(target, victim)
   assert(victim ~= local_player)
   if target == local_player then return end
   target.socket:Send("forget " .. tostring(victim.index) .. "\n")
end

--- Tell `target` everything it needs to know to achieve a synchronized world
--- state.
function update_world_for_player(target)
   if target == local_player then return end
   for n=1,#players do
      server_send_update_player(target, players[n])
   end
end

function server_send_update_player(target, victim)
   local message = ("update_player %i %.10f %.10f"):format(victim.index,
                                                           victim.x, victim.y)
   if target ~= victim then
      local t = {"x="..victim.input.x, "y="..victim.input.y}
      for k in pairs(victim.input) do
         if k ~= "x" and k ~= "y" then
            t[#t+1] = k
         end
      end
      message = message .. " " .. table.concat(t,",")
   end
   target.socket:Send(message.."\n")
end

function server_send_pew(target, projectile)
   local msg = ("pew %.10f %.10f %.10f %.10f %i\n")
   :format(projectile.x, projectile.y, projectile.vx, projectile.vy,
           projectile.color)
   target.socket:Send(msg)
end

function server_send_pows(index)
   for n=1,#players do
      local player = players[n]
      if player.socket and player ~= local_player then
         player.socket:Send("pow " .. index .. "\n")
      end
   end
end

--- Listens for new users, sends game state updates to clients,
--- receives and processes input from clients.
function server_action()
   accept_new_clients()
   sync_with_clients()
end
