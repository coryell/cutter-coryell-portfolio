-- Global game parameters.

default_camera_scale = 10 -- pixels / world unit
player_size = 10
projectile_size = 2
max_player_velocity = 5
projectile_velocity = 20
projectile_lifetime = 5 -- does not currently need to be same for all projs
