#
# Copyright (C) 2011  Antony Speranza, Leo Singer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Algorithms for optimal electromagnetic followup with multiple telescopes.
"""
__author__ = ('Antony Speranza <antony.speranza@ligo.org>', 'Leo Singer <leo.singer@ligo.org>')


import numpy as np
import healpy
from bayestar.convolve.multipole_openmp import convolve_altaz
from bayestar.convolve.examples import make_rect
from bayestar import sites, coord, misc
from bayestar.convolve.spatial import rotate_map, rotate_indices
from bayestar.telescope import Telescope


def is_in_fov(w, h, ra_fov, dec_fov, ra, dec):
    """Determine if a field of view with width w degrees and height h degrees,
    oriented at right ascension ra_fov and declination dec_fov, contains a source
    at right ascension ra and declination dec.  Right ascensions and declinations
    are in radians."""
    obj_xyz = healpy.ang2vec(0.5 * np.pi - dec, ra)
    R = healpy.rotator.euler_matrix_new(0., -(0.5 * np.pi - dec_fov), -ra_fov, Y=True)
    x, y, z = np.dot(obj_xyz, R)
    return (np.abs(x) <= np.sin(0.5 * np.deg2rad(w))) and (np.abs(y) <= np.sin(0.5 * np.deg2rad(h))) and (z > 0.)


def jd_tt_for_gps(gpstime):
    """Get the Julian day in TT for a given time in GPS seconds."""
    import novas.compat as nv
    return (gpstime
        + 19. # Convert from GPS to TAI
        + 32.184 # Convert from TAI to TT
    ) / 86400. + nv.julian_date(1980, 1, 6) # Add Julian day in TT of GPS epoch


def delta_t_for_gps(gpstime, delta_ut=0.):
    """Get the number of seconds between TT and UT1 for the given GPS time.
    For sub-second precision, provide delta_ut, the difference between UT1 and UTC."""
    import novas.compat as nv
    data = [
        (1980, 1, 1, 19),
        (1981, 7, 1, 20),
        (1982, 7, 1, 21),
        (1983, 7, 1, 22),
        (1985, 7, 1, 23),
        (1988, 1, 1, 24),
        (1990, 1, 1, 25),
        (1991, 1, 1, 26),
        (1992, 7, 1, 27),
        (1993, 7, 1, 28),
        (1994, 7, 1, 29),
        (1996, 1, 1, 30),
        (1997, 7, 1, 31),
        (1999, 1, 1, 32),
        (2006, 1, 1, 33),
        (2009, 1, 1, 34),
        (2012, 7, 1, 35)
    ]
    tai = gpstime + 19. + nv.julian_date(1980, 1, 6) * 86400.
    for year, month, day, ls in data[::-1]:
        if tai >= nv.julian_date(year, month, day) * 86400. + ls:
            leapseconds = ls
            break
    return leapseconds + 32.184 - delta_ut


def limb_angle(lon, lat, gpstime):
    """Return a HEALPix-indexed boolean mask that indicates positions on the
    celestial sphere that are accessible by a given telescope at a given time
    subject to sun and horizon interference."""

    import novas.compat as nv
    import novas.compat.eph_manager
    novas.compat.eph_manager.ephem_open()

    # Julian day in TT.
    jd_tt = jd_tt_for_gps(gpstime)

    # Compute delta_t, or TT-UT1.
    # Note: since we are not supplying tabulated delta_ut, or UT1-UTC,
    # the quantity calculated below is actually TT-UTC, which may differ
    # by +/-0.9 s.
    delta_t = delta_t_for_gps(gpstime)

    # Create object representing the Sun.
    sun = nv.make_object(0, 10, 'sun', None)

    # Create object representing the telescope's location on the surface of the Earth.
    # FIXME: bogus values for altitude, temperature, pressure.
    # NOVAS wants these fields, but I don't think that they are important
    # for our application.
    obs = nv.make_observer_on_surface(lat, lon, 0., 15., 1013.25)

    # Compute sun and observer position vectors in GCRS
    # (Geocentric Celestial Reference System).
    sun_xyz = nv.radec2vector(*nv.virtual_planet(jd_tt, sun))
    obs_xyz, _ = nv.geo_posvel(jd_tt, delta_t, obs)

    # Compute the solar inclination angle in degrees.
    limb_angle, _ = nv.limb_angle(sun_xyz, obs_xyz)

    # Done!
    return limb_angle


def geo2equ(lon, lat, gpstime):
    """Convert a geographic longitude and latitude (in degrees) to equatorial longitude and latitude."""

    import novas.compat as nv
    import novas.compat.eph_manager
    novas.compat.eph_manager.ephem_open()

    # Julian day in TT.
    jd_tt = jd_tt_for_gps(gpstime)

    # Compute delta_t, or TT-UT1.
    # Note: since we are not supplying tabulated delta_ut, or UT1-UTC,
    # the quantity calculated below is actually TT-UTC, which may differ
    # by +/-0.9 s.
    delta_t = delta_t_for_gps(gpstime)

    gmst = nv.sidereal_time(jd_tt - delta_t / 86400., 0., delta_t, gst_type=0, method=1)
    lon += gmst * 180. / 12.

    return lon, lat


def visible_mask(site, gpstime, nside):
    """Return a HEALPix-indexed boolean mask that indicates positions on the
    celestial sphere that are accessible by a given telescope at a given time
    subject to sun and horizon interference."""

    npix = healpy.nside2npix(nside)

    # If gpstime is None, do no horizon and sun interference checks.
    if gpstime is None:
        return np.ones(npix, dtype=bool)

    # Initialize output; default to nothing being visible.
    ret = np.zeros(npix, dtype=bool)

    # If the stupid sun is in the way, just return the array that we already have
    # (which is False everywhere).  This check tests if the observatory's local
    # solar time is between astronomical dusk and astronomical dawn.
    lon, lat = coord.str2lonlat(site['location'])
    if limb_angle(lon, lat, gpstime) > -18.:
        return ret

    # Convert telescope position to spherical polar coordinates.
    ra, dec = geo2equ(lon, lat, gpstime)
    phi = np.deg2rad(ra)
    theta = 0.5 * np.pi - np.deg2rad(dec)

    # Mask out directions that are not visible because the stupid ground is in the way.
    ret[misc.query_disc_inclusive(nside, theta, phi, 0.5 * np.pi)] = True

    # Done!
    return ret


def linear_in_log10_lightcurve(gpstime, a, b):
    """
        A linear lightcurve in magnitude, representing a function of the form
            m = a * log(gpstime) + b
    """
    return a * np.log(gpstime) / np.log(10.) + b


def absolute_mag_to_apparent_mag(abs_mag, dist):
    """
        Converts the given absolute magnitude to apparent magnitude when seen from the given distance, dist (Mpcs).
    """
    return abs_mag + 5 * np.log(dist * 10**5) / np.log(10)


def apparent_mag_to_absolute_mag(app_mag, dist):
    """
        Converts the given apparent magnitude to absolute magnitude when seen from the given distance, dist (Mpcs).
    """
    return app_mag - 5 * np.log(dist * 10**5) / np.log(10)


def greedy_solution(tel_keys, skymap, gpstime=None, lmax=None):
    """
    Employs a greedy algorithm to compute a solution to the optimization
    problem of pointing several telescopes at the same sky.  It returns a list of
    pointing directions for each telescope in tel_keys, as well as the total
    probability encolsed by all fields of view.

    Parameters:
        tel_keys:  list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        lmax: band limit for the multipole convolution algorithm.  The output
            of the convolution is sampled on an equiangular grid with angular
            spacing of (2*pi)/(2*lmax + 1)
    """

    nside = healpy.npix2nside(skymap.size)
    prob = 0.0
    used_telescopes = []
    pointings = []
    si = np.copy(skymap)


    for i, key in enumerate(tel_keys):

        site = sites[key]
        kern = make_rect(nside, 0.5 * site['w'], 0.5 * site['h'], deg=True)
        mask = visible_mask(site, gpstime, nside)
        masked_skymap = si * mask

        # If part of the skymap is visible to this telescope, skip it.
        if not masked_skymap.any():
            continue

        conv = convolve_altaz(masked_skymap, kern, lmax=lmax)
        max_idx = conv.argmax()
        prob += conv[max_idx]
        thm, phm = healpy.pix2ang(nside, max_idx)
        pointings.append((thm, phm))
        used_telescopes.append(key)

        si *= (1 - mask * rotate_map(kern, phm, thm, 0))

        del conv, kern, mask

    return used_telescopes, pointings, prob


def greedy_iterator(tel_keys, skymap, start_time, lightcurve, burst_size=1, interference=True, lmax=None):
    """
        Employs a greedy algorithm to compute a solution to the optimization    
            problem of pointing several telescopes at the same sky.  It returns a list of
            pointing directions for each telescope in tel_keys, as well as the total
            probability encolsed by all fields of view.
        
        Parameters:
            tel_keys:  list of strings specifying the telescopes to use.
            skymap: healpy array for the skymap.
            start_time: The gpstime at which telescope pointing is to start.
            lightcurve: a function of gpstime, returning the absolute magnitude of the source at that time
            burst_size: the minimum number of telescopes that point at any given event. If the number of
                telescopes that are ready to point is less than burst_size, they wait for additional
                telescopes to ready themselves. burst_size must be less than or equal to len(tel_keys).
            lmax: band limit for the multipole convolution algorithm.  The output
                of the convolution is sampled on an equiangular grid with angular
                spacing of (2*pi)/(2*lmax + 1)
        """
    
    nside = healpy.npix2nside(skymap.size)
    telescopes = []
    used_telescopes = []
    si = np.copy(skymap)
    pointings = []
    max_dists = []
    visibilities = []
    
    for key in tel_keys:
        telescopes.append(Telescope(key, start_time))
    
    #    counter = 1

    while True:
        
        #        print "looped {0}".format(counter)
        #        counter += 1
        
        go_times = [telescope.next_point_time for telescope in telescopes]
        go_times.sort()
        next_go_time = go_times[burst_size-1]

        if not si.any():
            raise StopIteration("skymap has been exhausted")

#print go_times
        next_telescopes = [telescope for telescope in telescopes if telescope.next_point_time <= next_go_time]
#print "next go time: {0}".format(next_go_time)
        
        source_absmag = lightcurve(next_go_time)
#print "source_absmag: {0}".format(source_absmag)
        for telescope in next_telescopes:
            #print "assigning {0}".format(telescope.key)
            telescope.assign_range(source_absmag)
            if interference:
                telescope.assign_visibility(nside, gpstime=next_go_time)
            else:
                telescope.assign_visibility(nside, gpstime=None)

        next_telescopes.sort(key=lambda telescope: telescope.volume, reverse=True)
#print "sorted next telescopes: {0}".format(next_telescopes)

        anything_pointed = False

        for telescope in next_telescopes:
            #print "now dealing with {0}".format(telescope.key)
            kern = make_rect(nside, 0.5 * telescope.w, 0.5 * telescope.h, deg=False)
            masked_skymap = si * telescope.visibility
            
            #print si.any(), telescope.visibility.any()
            
            if not masked_skymap.any():
                #print "{0} doesn't point".format(telescope.key)
                telescope.assign_next_point_time(next_go_time + 1.0)
                continue
            
            anything_pointed = True

            conv = convolve_altaz(masked_skymap, kern, lmax=lmax)
            max_idx = conv.argmax()
            thm, phm = healpy.pix2ang(nside, max_idx)
            telescope.assign_pointing((thm, phm), gpstime=next_go_time)
#print "history of {0}: {1}".format(telescope.key, telescope.history)

            used_telescopes.append(telescope.key)
            si *= (1 - telescope.visibility * rotate_map(kern, phm, thm, 0))
            pointings.append((thm, phm))
            max_dists.append(telescope.range)
            visibilities.append(np.copy(telescope.visibility))
            
#print telescope.last_point_time

        if anything_pointed:
            yield next_go_time, used_telescopes, si, pointings, max_dists, visibilities
            


def greedy_sorted_solution(tel_keys, skymap, gpstime=None, lmax=None, increasing=False):
    """
    Employs a greedy algorithm to find an approximate solution which maximizes
    the enclosed probability for multiple telescope pointings.  The order in
    which each telescope is pointed is determined by the telescope's field of
    view (w times h), going from largest to smallest.  It returns a list of
    pointing directions for each telescope in tel_keys, as well as the total
    probability enclosed by all fields of view.

    Parameters:
        tel_keys:  list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        lmax: band limit for the multipole convolution algorithm.  The output
            of the convolution is sampled on an equiangular grid with angular
            spacing of (2*pi)/(2*lmax + 1)
    """

    # sort telescopes by fov
    sorted_keys = sorted(tel_keys,
        key=lambda k: sites[k]['w'] * sites[k]['h'], reverse=(not increasing))

    # compute greedy solution
    used_telescopes, pointings, prob = greedy_solution(sorted_keys, skymap,
        gpstime=gpstime, lmax=lmax)
    # Special case if no telescopes get pointed.  The way we used zip(*...)
    # below, used_telescopes must be non-empty.
    if not used_telescopes:
        return [], [], 0.
    
    # return the solution to the original order
    pointings = dict(zip(used_telescopes, pointings))
    used_telescopes, pointings = zip(*[(key, pointings[key])
        for key in sorted_keys if key in pointings])
    return used_telescopes, pointings, prob


def noncoordinated_solution(tel_keys, skymap, gpstime=None, lmax=None):
    """
    Computes the telescope pointings using a non-cooperative algorithm which
    does not account for overlapping fields of view for the telescopes.  It
    returns a list of the pointings, as well as the total probability enclosed
    by all fields.

    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        lmax: band limit for the multipole convolution algorithm.  The output
        of the convolution is sampled on an equiangular grid with angular
        spacing of (2*pi)/(2*lmax + 1)
    """

    nside = healpy.npix2nside(skymap.size)
    used_telescopes = []
    pointings = []

    for key in tel_keys:
        site = sites[key]
        kern = make_rect(nside, 0.5 * site['w'], 0.5 * site['h'], deg=True)
        mask = visible_mask(site, gpstime, nside)
        masked_skymap = skymap * mask

        # If part of the skymap is visible to this telescope, skip it.
        if not masked_skymap.any():
            continue

        conv = convolve_altaz(masked_skymap, kern, lmax=lmax)
        max_idx = conv.argmax()
        thm, phm = healpy.pix2ang(nside, max_idx)
        pointings.append((thm, phm))
        used_telescopes.append(key)

    prob = p_any(used_telescopes, pointings, skymap, gpstime=gpstime)
    return used_telescopes, pointings, prob


def fmin_solution(tel_keys, skymap, gpstime=None, lmax=None, pointings0=None):
    """
    Employs conventional numerical optimization to compute a solution to the
    problem of pointing several telescopes at the same sky.  It returns a list of
    pointing directions for each telescope in tel_keys, as well as the total
    probability encolsed by all fields of view.

    Parameters:
        tel_keys:  list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        pointings0: initial guess (optional).
        lmax: band limit for the multipole convolution algorithm.  The output
            of the convolution is sampled on an equiangular grid with angular
            spacing of (2*pi)/(2*lmax + 1)
    """

    # Special case if no telescopes asked for.
    if not tel_keys:
        return [], [], 0.

    from scipy import optimize

    # If pointings0 is not provided, use output of "greedy sorted" algorithm
    # as initial state.
    if pointings0 is None:
        tel_keys, pointings0, prob = greedy_sorted_solution(tel_keys, skymap, gpstime=gpstime, lmax=lmax)

    # Cost function.
    def func(x):
        return -p_any(tel_keys, x.reshape(x.size / 2, 2), skymap, gpstime=gpstime)

    # Flatten pointings0 (I think fmin_bfgs may want a 1d state vector).
    x0 = np.array(pointings0).flatten()

    # Optimize away!
    xopt, fopt, _, _, _, _, _ = optimize.fmin_bfgs(func, x0, full_output=True, retall=False)

    # Return optimized parameters and cost.
    return tel_keys, xopt.reshape(xopt.size / 2, 2), -fopt


def anneal_solution(tel_keys, skymap, gpstime=None, lmax=None, pointings0=None):
    """
    Employs simulated annealing to compute a solution to the
    problem of pointing several telescopes at the same sky.  It returns a list of
    pointing directions for each telescope in tel_keys, as well as the total
    probability encolsed by all fields of view.

    Parameters:
        tel_keys:  list of strings specifying the telescopes to use.
        skymap: healpy array for the skymap.
        pointings0: initial guess (optional).
        lmax: band limit for the multipole convolution algorithm.  The output
            of the convolution is sampled on an equiangular grid with angular
            spacing of (2*pi)/(2*lmax + 1)
    """

    # Special case if no telescopes asked for.
    if not tel_keys:
        return [], [], 0.

    from scipy import optimize
    import sys

    # If pointings0 is not provided, use output of "greedy sorted" algorithm
    # as initial state.
    if pointings0 is None:
        tel_keys, pointings0, prob = greedy_sorted_solution(tel_keys, skymap, gpstime=gpstime, lmax=lmax)

    # Hack to import obscured module.  There is a function called "anneal" in
    # scipy.optimize which obscures the module called "scipy.optimize.anneal".
    # Luckily, we can retrieve the actual module object from sys.modules.
    anneal = sys.modules['scipy.optimize.anneal']

    # Cost function.
    def func(x):
        return -p_any(tel_keys, np.array(healpy.vec2ang(x)).T, skymap, gpstime=gpstime)

    # Subclass a cooling schedule so that random jumps get projected onto the
    # unit sphere.  This should be a benign modification to the proposed jump
    # as long as the jump is small.
    class unitsphere_sa(anneal.cauchy_sa):
        def update_guess(self, x0):
            x0 = super(unitsphere_sa, self).update_guess(x0)
            x0 /= np.expand_dims(np.sqrt((x0 * x0).sum(1)), 1).repeat(3, 1)
            return x0
    anneal.unitsphere_sa = unitsphere_sa

    # Convert pointings0 to an array of Cartesian coordinates.
    pointings0 = np.asarray(pointings0)
    x0 = healpy.ang2vec(pointings0[:, 0], pointings0[:, 1])

    # Anneal away!
    xopt, fopt, _, _, _, _, _ = optimize.anneal(func, x0,
        T0 = 1e-4, schedule='unitsphere', full_output=True)

    # Return optimized parameters and cost.
    return tel_keys, np.array(healpy.vec2ang(xopt)).T, -fopt


def p_any(tel_keys, pointings, skymap, gpstime=None):
    """
    Calculates the probability enclosed by the telescopes in tel_keys at the
    given pointings.  The kernels are assumed to be either 1 or 0 at each
    point, simplifying the calculation.

    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        pointings: a list of the same length as 'tel_keys', giving the pointing
            direction for each respective telescope.  Each pointing direction
            can be of the form (theta, phi) for an altazimuth rotation, or of
            the form (Phi_2, Theta, Phi_1) for a general Euler angle rotation.
        skymap: healpy array for the skymap.
    """

    nside = healpy.npix2nside(skymap.size)
    npix = healpy.nside2npix(nside)

    combined_kern = np.zeros(npix, dtype=bool)
    for k, p in zip(tel_keys, pointings):
        site = sites[k]
        w = 0.5 * np.deg2rad(site['w'])
        h = 0.5 * np.deg2rad(site['h'])
        kern = make_rect(nside, w, h, deg=False, dtype=bool)
        supported_radius = np.arcsin(np.sqrt(np.sin(w) ** 2 + np.sin(h) ** 2))
        mask = visible_mask(site, gpstime, nside)

        # If part of the skymap is visible to this telescope, skip it.
        if not mask.any():
            continue

        if len(p) == 2:
            euler_rotation = (p[1], p[0], 0)
        elif len(p) == 3:
            euler_rotation = p
        else:
            raise ValueError('Elements of pointings must be tuples of length '\
                             +'2 or 3')

        supported_indices = misc.query_disc_inclusive(nside, p[0], p[1], supported_radius)
        rotated_indices = rotate_indices(supported_indices, nside, *euler_rotation)
        combined_kern[supported_indices] |= kern[rotated_indices] & mask[supported_indices]

    return skymap[combined_kern].sum()


def p_all(tel_keys, pointings, skymap, gpstime=None):
    """
    Calculates the probability of observing the source in all telescopes in
    tel_keys.  

    Parameters:
        tel_keys: list of strings specifying the telescopes to use.
        pointings: a list of the same length as 'tel_keys', giving the pointing
            direction for each respective telescope.  Each pointing direction
            can be of the form (theta, phi) for an altazimuth rotation, or of
            the form (Phi_2, Theta, Phi_1) for a general Euler angle rotation.
        skymap: healpy array for the skymap.
    """

    nside = healpy.npix2nside(skymap.size)
    npix = healpy.nside2npix(nside)

    # load telescopes
    tels = np.asarray([sites[key] for key in tel_keys])

    #construct kernels 
    kerns = [make_rect(nside, 0.5 * st['w'], 0.5 * st['h'], deg=True) for st in tels]
    
    #TODO: write this calculation.
