/* Written by Cutter Coryell, July 9, 2013. */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include "cctk_WarnLevel.h"


/* C version of AMR_set_level_mask */
void AMR_set_level_mask2(CCTK_ARGUMENTS)
{
    
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /* Wipe away the level mask that exists */
  if (CCTK_EQUALS(radius_type, "shock"))
  {
    for(int k=0; k<cctk_lsh[2]; k++)
    {
      for(int j=0; j<cctk_lsh[1]; j++)
      {
        for(int i=0; i<cctk_lsh[0]; i++)
        {
          int vindex = CCTK_GFINDEX3D(cctkGH,i,j,k);
          level_mask[vindex] = 0.0;
        }
      }
    }

    CCTK_REAL dx = CCTK_DELTA_SPACE(0);
    CCTK_REAL dy = CCTK_DELTA_SPACE(1);
    CCTK_REAL dz = CCTK_DELTA_SPACE(2);

    /* now fill in level_mask */
    for(int k=0; k<cctk_lsh[2]; ++k)
    {
      for(int j=0; j<cctk_lsh[1]; ++j)
      {
        for(int i=0; i<cctk_lsh[0]; ++i)
        {
          int vindex = CCTK_GFINDEX3D(cctkGH,i,j,k);

          if (shock_strength[vindex] > shock_threshold)
          {
            /* radius of level_mask cube in index-units */
            int nx = (int)(cube_radius / dx);
            int ny = (int)(cube_radius / dy);
            int nz = (int)(cube_radius / dz);
            
            /* lower indices of cube (staying in bounds) */
            int lx = i - nx > 0 ? i - nx : 0;
            int ly = j - ny > 0 ? j - ny : 0;
            int lz = k - nz > 0 ? k - nz : 0;
            
            /* upper indices of cube (staying in bounds) */
            int ux = i + nx < cctk_lsh[0] ? i + nx : cctk_lsh[0] - 1;
            int uy = j + ny < cctk_lsh[1] ? j + ny : cctk_lsh[1] - 1;
            int uz = k + nz < cctk_lsh[2] ? k + nz : cctk_lsh[2] - 1;
          
            /* fill in the level_mask cube centered on this point */
            for (int k_ = lz; k_ <= uz; ++k_)
            {
              for(int j_ = ly; j_ <= uy; ++j_)
              {
                for(int i_ = lx; i_ <= ux; ++i_)
                {
                  int vindex_ = CCTK_GFINDEX3D(cctkGH,i_,j_,k_);
                  level_mask[vindex_] = mask_value;
                }
              }
            }
          }
        }
      }
    }
  }

  else if (CCTK_EQUALS(radius_type, "sphere"))
  {
    for(int k=0; k<cctk_lsh[2]; k++)
    {
      for(int j=0; j<cctk_lsh[1]; j++)
      {
        for(int i=0; i<cctk_lsh[0]; i++)
        {
          int vindex = CCTK_GFINDEX3D(cctkGH,i,j,k);

# if 0          
          if (level_mask[vindex] < 0 || 100 < level_mask[vindex])
          {
            CCTK_WARN(CCTK_WARN_ABORT, "level mask contains strange values");
          }
# endif
          
          level_mask[vindex] = max_radius / fmax(r[vindex], min_radius);
        }
      }
    }
  }

  else if (CCTK_EQUALS(radius_type, "annulus"))
  {
    for(int k=0; k<cctk_lsh[2]; k++)
    {
      for(int j=0; j<cctk_lsh[1]; j++)
      {
        for(int i=0; i<cctk_lsh[0]; i++)
        {
          int vindex = CCTK_GFINDEX3D(cctkGH,i,j,k);

# if 0          
          if (level_mask[vindex] < 0 || 100 < level_mask[vindex])
          {
            CCTK_WARN(CCTK_WARN_ABORT, "level mask contains strange values");
          }
# endif
          
          if (min_radius < r[vindex] && r[vindex] < max_radius)
          {
            level_mask[vindex] = mask_value;
          }
          else
          {
            level_mask[vindex] = 0.0;
          }
        }
      }
    }
  }
}
