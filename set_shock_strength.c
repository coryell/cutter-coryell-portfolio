/* Written by Cutter Coryell, July 9, 2013. */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>
#include "cctk_WarnLevel.h"

/* shock_strength is the magnitude of the gradient of AMR_tracked_quantity
 * divided by AMR_tracked_quantity
 */
void AMR_set_shock_strength(CCTK_ARGUMENTS)
{
    
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_REAL *quantity;

  if (CCTK_EQUALS(AMR_tracked_quantity, "density"))
  {
    quantity = rho;
  }
  else if (CCTK_EQUALS(AMR_tracked_quantity, "pressure"))
  {
    quantity = press;
  }
  else if (CCTK_EQUALS(AMR_tracked_quantity, "epsilon"))
  {
    quantity = eps;
  }
  else if (CCTK_EQUALS(AMR_tracked_quantity, "temperature"))
  {
    quantity = temperature;
  }
  else if (CCTK_EQUALS(AMR_tracked_quantity,"entropy"))
  {
    quantity = entropy;
  }
  else
  {
    CCTK_WARN(CCTK_WARN_ABORT, "AMR_tracked_quantity out of range");
  }

  int nx = cctk_lsh[0];
  int ny = cctk_lsh[1];
  int nz = cctk_lsh[2];
  CCTK_REAL two_dx = 2.0 * CCTK_DELTA_SPACE(0);
  CCTK_REAL two_dy = 2.0 * CCTK_DELTA_SPACE(1);
  CCTK_REAL two_dz = 2.0 * CCTK_DELTA_SPACE(2);
  int center, xleft, xright, yleft, yright, zleft, zright;
  CCTK_REAL xder, yder, zder;
  
  for(int k = 1; k < nz - 1; ++k)
  {
    for(int j = 1; j < ny - 1; ++j)
    {
      for(int i = 1; i < nx - 1; ++i)
      {
        center = CCTK_GFINDEX3D(cctkGH, i, j, k);
        xleft = CCTK_GFINDEX3D(cctkGH, i-1, j, k);
        xright = CCTK_GFINDEX3D(cctkGH, i+1, j, k);
        yleft = CCTK_GFINDEX3D(cctkGH, i, j-1, k);
        yright = CCTK_GFINDEX3D(cctkGH, i, j+1, k);
        zleft = CCTK_GFINDEX3D(cctkGH, i, j, k-1);
        zright = CCTK_GFINDEX3D(cctkGH, i, j, k+1);

        xder = (quantity[xleft] - quantity[xright]) / two_dx;
        yder = (quantity[yleft] - quantity[yright]) / two_dy;
        zder = (quantity[zleft] - quantity[zright]) / two_dz;
        shock_strength[center] = sqrt(xder*xder + yder*yder + zder*zder)
                                                     / quantity[center];
      }
    }
  }
}
